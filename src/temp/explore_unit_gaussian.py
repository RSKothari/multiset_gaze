#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 25 10:43:50 2021

@author: rakshit
"""

import pickle
import numpy as np
import matplotlib.pyplot as plt


def generate_asymmetrical_dist(means, spreads, weight):
    num_samples = 100000
    weight = weight/np.sum(weight)
    data_1 = np.random.normal(means[0], spreads[0], int(weight[0]*num_samples))
    data_2 = np.random.normal(means[1], spreads[1], int(weight[1]*num_samples))
    data_3 = np.random.normal(means[2], spreads[2], int(weight[2]*num_samples))
    return np.concatenate([data_1, data_2, data_3])


if __name__ == "__main__":
    data = [generate_asymmetrical_dist([1, 1.5, -3], [5, 3, 1.5], [2, 4, 6]),
            generate_asymmetrical_dist([5, -1.5, 2], [3, 1.5, 5], [8, 2, 3]),
            generate_asymmetrical_dist([8, -8, 2], [3, 1.5, 5], [3, 9, 2]),
            generate_asymmetrical_dist([-4, 0, 4], [8, 8, 8], [1, 1, 1])]

    with open('asymm_dist.pkl', 'wb') as f:
        pickle.dump(data, f)

    with open('asymm_dist.pkl', 'rb') as f:
        data = pickle.load(f)

    n_bins = 100
    hist = [np.histogram(ele, bins=n_bins) for ele in data]

    x_range = [ele[1][:-1] + np.diff(ele[1]) for ele in hist]

    fig, axs = plt.subplots(nrows=1, ncols=2)

    axs[0].grid('on')
    axs[1].grid('on')

    for idx, ele in enumerate(hist):
        axs[0].plot(x_range[idx],
                    hist[idx][0]/np.sum(hist[idx][0]),
                    alpha=0.6, linewidth=2, label=r"$\mathcal{d}_%d$"%(idx+1))
    axs[0].set_xlabel('Features', fontsize=12)
    axs[0].set_ylabel('Prob', fontsize=12)
    axs[0].set_xlim([-12, 12])
    axs[0].legend()

    # %% Normalize the distribution
    data_norm = [(ele - ele.mean())/ele.std() for ele in data]
    hist_norm = [np.histogram(ele, bins=n_bins) for ele in data_norm]

    x_range_norm = [ele[1][:-1] + np.diff(ele[1]) for ele in hist_norm]

    for idx, ele in enumerate(hist_norm):
        axs[1].plot(x_range_norm[idx],
                    hist_norm[idx][0]/np.sum(hist_norm[idx][0]),
                    alpha=0.6, linewidth=2, label=r"$\mathcal{d}_%d$"%(idx+1))
    axs[1].set_xlabel('Z scores', fontsize=12)
    axs[1].legend()

    fig.savefig('thesis-BN_vs_IN.png', transparent=True, dpi=600, bbox_inches='tight')