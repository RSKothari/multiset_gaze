#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 03:41:08 2021

@author: rsk3900
"""

import os
import cv2
import sys
import argparse

import numpy as np
import pandas as pd
import multiprocessing as mp

from skimage import draw

sys.path.append('..')
from helperfunctions.helperfunctions import plot_segmap_ellpreds

def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path_data', type=str, default='/data/TEyeDSSingleFiles/Dikablis',
                        help='path to TEyeD Dikablis eye videos')
    parser.add_argument('--path_output', type=str, default='/data/TEyeDSSingleFiles/Dikablis/data',
                        help='path to extracted output')
    args = parser.parse_args()
    return args


def process_entry(args, ):

    # Read text file
    iris_ellipses = pd.read_csv(args['path_annot']+'iris_eli.txt',
                                error_bad_lines=False,
                                delimiter=';').to_numpy()
    pupil_ellipses = pd.read_csv(args['path_annot']+'pupil_eli.txt',
                                 error_bad_lines=False,
                                 delimiter=';').to_numpy()

    iris_validity = pd.read_csv(args['path_annot']+'iris_validity.txt',
                                error_bad_lines=False,
                                delimiter=';').to_numpy()

    pupil_validity = pd.read_csv(args['path_annot']+'pupil_validity.txt',
                                 error_bad_lines=False,
                                 delimiter=';').to_numpy()

    # Read video frame by frame
    vid_obj = cv2.VideoCapture(args['path_video'])
    width = vid_obj.get(cv2.CAP_PROP_FRAME_WIDTH)  # float `width`
    height = vid_obj.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float `height`

    ret = True  # Start the loop
    fr_idx = 0

    while ret:
        ret, frame = vid_obj.read()
        if not ret:
            break

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        fr_idx += 1

        out_dict = {}

        # Generate masks
        out_dict['iris_ellipse'] = iris_ellipses[fr_idx, 1:-1]
        out_dict['pupil_ellipse'] = pupil_ellipses[fr_idx, 1:-1]

        # Convert to EllSeg style
        out_dict['iris_ellipse'] = np.roll(out_dict['iris_ellipse'], shift=-1)
        out_dict['pupil_ellipse'] = np.roll(out_dict['pupil_ellipse'], shift=-1)

        # Find semi-major and minor axis
        out_dict['pupil_ellipse'][2:4] = out_dict['pupil_ellipse'][2:4]/2
        out_dict['iris_ellipse'][2:4] = out_dict['iris_ellipse'][2:4]/2

        # Shift angle by 90 and flip minor/major axis
        out_dict['pupil_ellipse'][-1] = np.deg2rad(out_dict['pupil_ellipse'][-1]-90)
        out_dict['iris_ellipse'][-1] = np.deg2rad(out_dict['iris_ellipse'][-1]-90)

        out_dict['pupil_ellipse'][[2, 3]] = out_dict['pupil_ellipse'][[3, 2]]
        out_dict['iris_ellipse'][[2, 3]] = out_dict['iris_ellipse'][[3, 2]]

        if np.any(out_dict['iris_ellipse'] == -1) or \
           np.any(out_dict['pupil_ellipse'] == -1):
            continue

        [rr_i, cc_i] = draw.ellipse(round(out_dict['iris_ellipse'][1]),
                                    round(out_dict['iris_ellipse'][0]),
                                    round(out_dict['iris_ellipse'][3]),
                                    round(out_dict['iris_ellipse'][2]),
                                    shape=(int(height), int(width)),
                                    rotation=-out_dict['iris_ellipse'][4])

        [rr_p, cc_p] = draw.ellipse(round(out_dict['pupil_ellipse'][1]),
                                    round(out_dict['pupil_ellipse'][0]),
                                    round(out_dict['pupil_ellipse'][3]),
                                    round(out_dict['pupil_ellipse'][2]),
                                    shape=(int(height), int(width)),
                                    rotation=-out_dict['pupil_ellipse'][4])

        # Save frame and masks
        out_dict['mask'] = np.zeros((int(height), int(width)), dtype=np.int)
        out_dict['mask'][rr_i, cc_i] = 1
        out_dict['mask'][rr_p, cc_p] = 2

        # Plot groundtruth
        frame_overlayed_with_op = plot_segmap_ellpreds(frame,
                                                       out_dict['mask'],
                                                       out_dict['pupil_ellipse'],
                                                       out_dict['iris_ellipse'])

        # Write out overlayed frame
        path_verification_frame = os.path.join(args['path_data'],
                                               'verification',
                                               args['vid_name'],
                                               '{:08}.png'.format(fr_idx))

        os.makedirs(os.path.dirname(path_verification_frame), exist_ok=True)

        # Write out image
        cv2.imwrite(path_verification_frame,
                    frame_overlayed_with_op[:, :, ::-1])

    vid_obj.release()

if __name__ == '__main__':

    args = vars(make_args())
    path_videos = os.path.join(args['path_data'], 'VIDEOS')
    path_annots = os.path.join(args['path_data'], 'ANNOTATIONS')
    list_videos = os.listdir(path_videos)

    # pool = mp.Pool(mp.cpu_count())

    for vid_name_ext in list_videos:

        if 'DikablisT_22_5' not in vid_name_ext:
            args['vid_name'] = os.path.splitext(vid_name_ext)[0]
            args['path_video'] = os.path.join(path_videos, vid_name_ext)
            args['path_annot'] = os.path.join(path_annots, vid_name_ext)

            # pool.apply_async()
            process_entry(args, )





